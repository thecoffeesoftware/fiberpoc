import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {

	public static AtomicInteger fiberLimit = new AtomicInteger(10000000);
	public static AtomicInteger currentFiberCount = new AtomicInteger(0);

	public static void main(String[] args) {
		System.out.println("FiberCount: " + fiberLimit);
		LocalDateTime begin = LocalDateTime.now();
		LocalDateTime fiberCreation;
		LocalDateTime fiberFinish;
		try (FiberScope fiberScope = FiberScope.open()) {
			for (int i = 0; i < fiberLimit.get(); i++) {
				fiberScope.schedule(new TestRunnable());
				currentFiberCount.incrementAndGet();
			}
			fiberCreation = LocalDateTime.now();
		}
		fiberFinish = LocalDateTime.now();
		System.out.println(begin + " program begin!");
		System.out.println(fiberCreation + " fibers created!");
		System.out.println(fiberFinish + " fibers finished!");
		System.out.println("Creation time in millis: " + ChronoUnit.MILLIS.between(begin, fiberCreation));
		System.out.println("Execution time in millis: " + ChronoUnit.MILLIS.between(fiberCreation, fiberFinish));
		System.out.println("Run time in millis: " + ChronoUnit.MILLIS.between(begin, fiberFinish));
	}
}
